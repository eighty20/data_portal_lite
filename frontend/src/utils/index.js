export const statusToAlertType = (status) => {
    const alertTypes = {
        request: "info",
        response: "success",
        error: "danger"
    }
    return alertTypes[status]
}
