import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import reducer from './reducers'

const configureStore = () => {
    const store = createStore(
        reducer,
        composeWithDevTools(
            applyMiddleware(thunk)
        )
    )

    if (process.env.NODE_ENV !== "production") {
        if (module.hot) {
            module.hot.accept('./reducers', () => {
                store.replaceReducer(reducer)
            })
        }
    }

    return store
}

export default configureStore
