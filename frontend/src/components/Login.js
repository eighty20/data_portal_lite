import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Form, FormGroup, Label, Col, Input, Button, Alert } from 'reactstrap'
import { Link } from 'react-router-dom'

import * as actions from '../actions'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: "",
            password: "",
        }
    }

    onChangeHandler = (e) => {
        const { clearAlerts, alertType } = this.props
        e.preventDefault()
        this.setState({
            [e.target.id]: e.target.value
        })
        if (alertType) {
            clearAlerts()
        }
    }

    onSubmitHandler = () => {
        const { email, password } = this.state
        const { loginUser, loginUserError } = this.props
        if (email && password) {
            const data = {
                email: email,
                password: password
            }
            loginUser(data)
        }
        else {
            loginUserError("Login with email and password")
        }
    }

    render() {
        const { alertType, message } = this.props
        return (
            <div>
                <h3>Login</h3>
                <Form>
                    <FormGroup row>
                        <Label for="email" md={3}>Email</Label>
                        <Col md={6}>
                            <Input
                                type="email"
                                name="email"
                                id="email"
                                onChange={this.onChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="password" md={3}>Password</Label>
                        <Col md={6}>
                            <Input
                                type="password"
                                name="password"
                                id="password"
                                onChange={this.onChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md={{size: 2, offset: 3}}>
                            <Button block color="primary"
                                onClick={this.onSubmitHandler}
                                className="mb-2"
                            >
                                Login
                            </Button>
                        </Col>
                        <Col md={{size: 2, offset: 2}}>
                            <Button block
                                tag={Link}
                                to="/register"
                                className="mb-2"
                            >
                                Register
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
                { alertType ?
                    <Alert color={alertType}>{message}</Alert> :
                    <Alert color="info invisible">Placeholder</Alert>
                }
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const { status, message } = state.dataManagement.login
    let alertType
    switch (status){
        case "request":
            alertType = "info"
            break
        case "response":
            alertType = "success"
            break
        case "error":
            alertType = "danger"
            break
        default:
            alertType = ""
    }
    return {
        alertType,
        message
    }
}

export default connect(mapStateToProps, actions)(Login)
