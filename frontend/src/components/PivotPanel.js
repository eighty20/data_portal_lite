import React, { Component } from 'react'
import { Button, Table, Alert } from 'reactstrap'

import PivotTable from './PivotTable'

import { backendHost } from '../actions/backend'

class PivotPanel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pivot: null,
            alertType: "",
            message: ""
        }
    }

    getPivot = () => {
        const { rowField, colField } = this.props
        if (rowField && colField && (rowField !== colField)) {
            const url = new URL(`${backendHost}/pivot`)
            url.searchParams.append('database', 'labour_fs')
            url.searchParams.append('table', 'lfs2018q1')
            url.searchParams.append('row_field', this.props.rowField)
            url.searchParams.append('col_field', this.props.colField)
            fetch(url, {
                method: 'GET',
                headers:{
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                mode: 'cors',
            })
            .then(response => {
                if (response.ok) {
                    return response
                }
                else {
                    this.setState({
                        message: response.message,
                        alertType: 'danger'
                    })
                }
            })
            .then(response => response.json())
            .then(response => {
                this.setState({
                    pivot: response.payload.entities.pivot,
                    message: '',
                    alertType: ''
                })
            })
            .catch(error => {
                this.setState({
                    message: error.message,
                    alertType: 'danger'
                })
            })
            this.setState({
                message: "Fetching pivot",
                alertType: 'info'
            })
        }
        else {
            this.setState({
                message: "Select a valid row and column",
                alertType: 'danger'
            })
        }
    }

    render() {
        const { rowFieldDesc, colFieldDesc } = this.props
        const { pivot, alertType, message } = this.state
        return (
            <div>
                <h5 className="clearfix">
                    Pivot
                    <Button
                        size="sm"
                        className="pull-right"
                        color="primary"
                        onClick={this.getPivot}
                    >
                        Go
                    </Button>
                </h5>
                <Table striped bordered>
                    <tbody>
                        <tr>
                            <th width="30%">Row Field</th>
                            <td>{rowFieldDesc}</td>
                        </tr>
                        <tr>
                            <th>Col Field</th>
                            <td>{colFieldDesc}</td>
                        </tr>
                    </tbody>
                </Table>
                { alertType &&
                    <Alert color={alertType}>{message}</Alert>
                }
                <PivotTable pivot={pivot}/>
            </div>
        )
    }
}

export default PivotPanel
