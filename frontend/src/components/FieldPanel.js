import React, { Component } from 'react'
import { Alert } from 'reactstrap'

import Tree from './Tree'

import { backendHost } from '../actions/backend'

class FieldPanel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            fieldTree: {
                0: {
                    name: "Root",
                    isLeaf: false,
                    children: []
                }
            },
            alertType: "",
            message: ""
        }
    }

    componentDidMount() {
        const url = new URL(`${backendHost}/fieldTree`)
        url.searchParams.append('database', 'labour_fs')
        url.searchParams.append('table', 'lfs2018q1')
        fetch(url, {
            method: 'GET',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        })
        .then(response => {
            if (response.ok) {
                return response
            }
            else {
                this.setState({
                    message: response.message,
                    alertType: 'danger'
                })
            }
        })
        .then(response => response.json())
        .then(response => {
            this.setState({
                fieldTree: response.payload.entities.field_tree,
                message: '',
                alertType: ''
            })
        })
        .catch(error => {
            this.setState({
                message: error.message,
                alertType: 'danger'
            })
        })
        this.setState({
            message: "Fetching fields",
            alertType: 'info'
        })
    }

    render() {
        const { onSelectField } = this.props
        const { fieldTree, alertType, message } = this.state
        return (
            <div>
                <h5>Fields</h5>
                <Tree tree={fieldTree} onClickHandler={onSelectField}/>
                { alertType &&
                    <Alert color={alertType}>{message}</Alert>
                }
            </div>
        )
    }
}

export default FieldPanel
