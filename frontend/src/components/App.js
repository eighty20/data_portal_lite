import React, { Component } from 'react'
import { Container, Row, Col, Card } from 'reactstrap'
import { Route, Switch, Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import Topbar from './Topbar'
import Home from './Home'
import DataPortalLite from './DataPortalLite'
import Mockup from './Mockup'
import Login from './Login'
import Register from './Register'
import UserAccount from './UserAccount'
import Admin from './Admin'

const ProtectedRoute = ({ isAllowed, ...props }) => (
    isAllowed
    ? <Route {...props}/>
    : <Redirect to="/login"/>
)

const AntiProtectedRoute = ({ isAllowed, ...props }) => (
    isAllowed
    ? <Redirect to="/"/>
    : <Route {...props}/>
)

class App extends Component {
    render() {
        const { loggedIn, loggedInAdmin } = this.props
        return (
            <div>
                <Topbar/>
                <Container fluid={true}>
                    <Row>
                        <Col>
                            <main>
                                <Card body className="mt-2">
                                    <Switch>
                                        <Route exact path="/" component={Home}/>
                                        <Route path="/dataPortalLite" component={DataPortalLite}/>
                                        <Route path="/mockup" component={Mockup}/>
                                        <AntiProtectedRoute isAllowed={loggedIn} path="/login" component={Login}/>
                                        <AntiProtectedRoute path="/register" component={Register}/>
                                        <ProtectedRoute isAllowed={loggedIn} path="/account" component={UserAccount}/>
                                        <ProtectedRoute isAllowed={loggedInAdmin} path="/admin" component={Admin}/>
                                    </Switch>
                                </Card>
                            </main>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const { loggedIn, loggedInAdmin } = state.auth
    return {
        loggedIn,
        loggedInAdmin
    }

}
export default withRouter(connect(mapStateToProps)(App))
