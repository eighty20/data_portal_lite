import React, { Component } from 'react'
import { ListGroup, ListGroupItem, ButtonGroup, Button, Collapse } from 'reactstrap'
import { map } from 'lodash'

class Node extends Component {
    constructor(props) {
        super(props)
        this.state = { collapse: true }
    }

    toggle = () => {
        this.setState({ collapse: !this.state.collapse })
    }

    render() {
        const { tree, nodeId, onClickHandler } = this.props
        const node = tree[nodeId]
        return ([
            <ListGroupItem key="node" className={ node.isLeaf ? "bg-light" :"bg-light"}>
                { !node.isLeaf &&
                    <a onClick={this.toggle} className="toggle-collapse">
                        <i
                            className={ this.state.collapse ? "fas fa-angle-down" : "fas fa-angle-up" }
                        />
                    </a>
                } {" "}
                { node.name }
                { node.isLeaf &&
                    <ButtonGroup size="sm" className="pull-right">
                        <Button outline onClick={() => onClickHandler("row", node.field, node.name)}>R</Button>
                        <Button outline onClick={() => onClickHandler("col", node.field, node.name)}>C</Button>
                    </ButtonGroup>
                }
            </ListGroupItem>,
            !node.isLeaf &&
            <Collapse isOpen={!this.state.collapse} key="children">
                <ListGroupItem>
                    <ListGroup>
                        { map(node.children, (childId) => (
                            <Node key={childId} tree={tree} nodeId={childId} onClickHandler={onClickHandler}/>
                        ))}
                    </ListGroup>
                </ListGroupItem>
            </Collapse>
        ])
    }
}

const Tree = ({tree, onClickHandler}) => (
    <ListGroup>
        { map(tree[0].children, (childId) => (
            <Node key={childId} tree={tree} nodeId={childId} onClickHandler={onClickHandler}/>
        ))}
    </ListGroup>
)

export default Tree
