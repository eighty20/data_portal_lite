import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Form, FormGroup, Label, Col, Input, Button, Alert } from 'reactstrap'

import * as actions from '../actions'

class UserAccount extends Component {
    constructor(props) {
        super(props)
        const { firstName, lastName, email } = props
        this.state = {
            firstName,
            lastName,
            email,
            oldPassword: "",
            newPassword: "",
            confirmPassword: "",
            editMode: false,
        }
    }

    onChangeHandler = (e) => {
        const { clearAlerts, alertType } = this.props
        e.preventDefault()
        this.setState({
            [e.target.id]: e.target.value,
        })
        if (alertType) {
            clearAlerts()
        }
    }

    onToggleEditMode = () => {
        const { firstName, lastName, email } = this.props
        const { editMode } = this.state
        this.setState({
            editMode: !editMode,
            firstName: firstName,
            lastName: lastName,
            email: email
        })
    }

    onEditUserHandler = () => {
        const { firstName, lastName, email } = this.state
        const { id, editUser, editUserError } = this.props
        if (firstName && lastName) {
            const data = {
                id,
                first_name: firstName,
                last_name: lastName,
                email: email,
            }
            editUser(data)
            this.setState({
                editMode: false
            })
        }
        else {
            editUserError("All compulsory fields indicated with * must be filled in")
        }
    }

    onChangePasswordHandler = () => {
        const { oldPassword, newPassword, confirmPassword } = this.state
        const { id, changePassword, changePasswordError } = this.props
        if (oldPassword && newPassword && confirmPassword) {
            if (newPassword === confirmPassword) {
                const data = {
                    id,
                    old_password: oldPassword,
                    new_password: newPassword,
                    confirm_password: confirmPassword,
                }
                changePassword(data)
            }
            else {
                changePasswordError("Password and confirmation must match")
            }
        }
        else {
            changePasswordError("All compulsory fields indicated with * must be filled in")
        }
    }

    render() {
        const { alertType, message } = this.props
        const {
            oldPassword, newPassword,
            confirmPassword, editMode
        } = this.state
        return (
            <div>
                <h3>UserAccount</h3>
                <Form>
                    <FormGroup row>
                        <Label for="firstName" md={3}>First Name *</Label>
                        <Col md={6}>
                            <Input
                                name="firstName"
                                id="firstName"
                                onChange={this.onChangeHandler}
                                value={this.state.firstName}
                                disabled={!editMode}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="LastName" md={3}>Last Name</Label>
                        <Col md={6}>
                            <Input
                                name="lastName"
                                id="lastName"
                                onChange={this.onChangeHandler}
                                value={this.state.lastName}
                                disabled={!editMode}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="email" md={3}>Email *</Label>
                        <Col md={6}>
                            <Input
                                type="email"
                                name="email"
                                id="email"
                                onChange={this.onChangeHandler}
                                value={this.state.email}
                                disabled={!editMode}
                            />
                        </Col>
                    </FormGroup>
                        { editMode ?
                            <FormGroup row>
                                <Col md={{size: 3, offset: 3}}>
                                    <Button
                                        block
                                        color="primary"
                                        onClick={this.onEditUserHandler}
                                        className="mb-2"
                                    >
                                        Submit
                                    </Button>
                                </Col>
                                <Col md={{size: 3}}>
                                    <Button
                                        block
                                        onClick={this.onToggleEditMode}
                                        className="mb-2"
                                    >
                                        Cancel
                                    </Button>
                                </Col>
                            </FormGroup> :
                            <FormGroup row>
                                <Col md={{size: 3, offset: 3}}>
                                    <Button
                                        block
                                        color="primary"
                                        onClick={this.onToggleEditMode}
                                    >
                                        Edit Account
                                    </Button>
                                </Col>
                            </FormGroup>
                        }
                    <FormGroup row>
                        <Label for="oldPassword" md={3}>Old Password *</Label>
                        <Col md={6}>
                            <Input
                                type="password"
                                name="oldPassword"
                                id="oldPassword"
                                onChange={this.onChangeHandler}
                                value={oldPassword}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="newPassword" md={3}>New Password *</Label>
                        <Col md={6}>
                            <Input
                                type="password"
                                name="newPassword"
                                id="newPassword"
                                onChange={this.onChangeHandler}
                                value={newPassword}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="confirmPassword" md={3}>Confirm Password *</Label>
                        <Col md={6}>
                            <Input
                                type="password"
                                name="confirmPassword"
                                id="confirmPassword"
                                onChange={this.onChangeHandler}
                                value={confirmPassword}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md={{size: 3, offset: 3}}>
                            <Button
                                block
                                color="primary"
                                onClick={this.onChangePasswordHandler}
                            >
                                Change Password
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
                { alertType ?
                    <Alert color={alertType}>{message}</Alert> :
                    <Alert color="info invisible">Placeholder</Alert>
                }
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const { status, message } = state.dataManagement.account
    const { loggedInUser } = state.auth
    const { id, first_name, last_name, email } = state.entities.users[loggedInUser]
    let alertType
    switch (status){
        case "request":
            alertType = "info"
            break
        case "response":
            alertType = "success"
            break
        case "error":
            alertType = "danger"
            break
        default:
            alertType = ""
    }
    return {
        alertType,
        message,
        id,
        firstName: first_name,
        lastName: last_name,
        email: email,
    }
}

export default connect(mapStateToProps, actions)(UserAccount)
