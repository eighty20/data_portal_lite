import React from 'react'
import { Table } from 'reactstrap'
import { map } from 'lodash'

const PivotTable = ({pivot}) => (
    pivot &&
    <Table responsive striped bordered>
        <thead>
            <tr>
                <th>{pivot.table}</th>
                { map(pivot.cols, (level) => (
                    <th key={level}>{level}</th>
                ))}
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            { map(pivot.rows, (level, row_no) => (
                <tr key={row_no}>
                    <th>{level}</th>
                    { map(pivot.data[row_no], (value, col_no) => (
                        <td key={col_no}>{value}</td>
                    ))}
                    <td>{pivot.totals.rows[row_no]}</td>
                </tr>
            ))}
            <tr>
                <th>Total</th>
                { map(pivot.totals.cols, (value, col_no) => (
                    <th key={col_no}>{value}</th>
                ))}
                <th>{pivot.totals.grand}</th>
            </tr>
        </tbody>
    </Table>
)

export default PivotTable
