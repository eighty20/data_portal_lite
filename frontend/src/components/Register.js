import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Form, FormGroup, Label, Col, Input, Button, Alert } from 'reactstrap'

import * as actions from '../actions'

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            confirmPassword: "",
        }
    }

    onChangeHandler = (e) => {
        const { clearAlerts, alertType } = this.props
        e.preventDefault()
        this.setState({
            [e.target.id]: e.target.value,
        })
        if (alertType) {
            clearAlerts()
        }
    }

    onSubmitHandler = () => {
        const { firstName, lastName, email, password, confirmPassword } = this.state
        const { registerUser, registerUserError } = this.props
        if (firstName && email && password && confirmPassword) {
            if (password === confirmPassword) {
                const data = {
                    first_name: firstName,
                    last_name: lastName,
                    email: email,
                    password: password
                }
                registerUser(data)
            }
            else {
                registerUserError("Password and confirmation must match")
            }
        }
        else {
            registerUserError("All compulsory fields indicated with * must be filled in")
        }
    }

    render() {
        const { alertType, message } = this.props
        return (
            <div>
                <h3>Register</h3>
                <Form>
                    <FormGroup row>
                        <Label for="firstName" md={3}>First Name *</Label>
                        <Col md={6}>
                            <Input name="firstName" id="firstName" onChange={this.onChangeHandler}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="LastName" md={3}>Last Name</Label>
                        <Col md={6}>
                            <Input name="lastName" id="lastName" onChange={this.onChangeHandler}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="email" md={3}>Email *</Label>
                        <Col md={6}>
                            <Input type="email" name="email" id="email" onChange={this.onChangeHandler}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="password" md={3}>Password *</Label>
                        <Col md={6}>
                            <Input type="password" name="password" id="password" onChange={this.onChangeHandler}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="confirmPassword" md={3}>Confirm Password *</Label>
                        <Col md={6}>
                            <Input type="password" name="confirmPassword" id="confirmPassword" onChange={this.onChangeHandler}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col md={{size: 2, offset: 3}}>
                            <Button block color="primary" onClick={this.onSubmitHandler}>Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
                { alertType ?
                    <Alert color={alertType}>{message}</Alert> :
                    <Alert color="info invisible">Placeholder</Alert>
                }
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const { status, message } = state.dataManagement.register
    let alertType
    switch (status){
        case "request":
            alertType = "info"
            break
        case "response":
            alertType = "success"
            break
        case "error":
            alertType = "danger"
            break
        default:
            alertType = ""
    }
    return {
        alertType,
        message
    }
}

export default connect(mapStateToProps, actions)(Register)
