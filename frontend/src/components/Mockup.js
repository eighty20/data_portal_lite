import React, { Component } from 'react'
import { Row, Col, Table } from 'reactstrap'
import { map } from 'lodash'

const pivot = {
    table: "Table Name: Period",
    rows: ["Row Level 1", "Row Level 2"],
    cols: ["Col Level 1", "Col Level 2", "Col Level 3"],
    data: [
        [1, 2, 3],
        [4, 5, 6]
    ],
    totals: {
        cols: [5, 7, 8],
        rows: [6, 15],
        grand: 21
    }
}

const fieldTree = {
    0: {
        name: "Root",
        isLeaf: false,
        children: [1, 2]
    },
    1: {
        name: "Branch 1",
        isLeaf: false,
        children: [3],
    },
    2: {
        name: "Branch 2",
        isLeaf: false,
        children: [4, 6],
    },
    3: {
        name: "Branch 1.1",
        isLeaf: false,
        children: [5],
    },
    4: {
        name: "Leaf 2.1",
        isLeaf: true,
        field: "field_1",
    },
    5: {
        name: "Leaf 1.1.1",
        isLeaf: true,
        field: "field_2",
    },
    6: {
        name: "Leaf 2.2",
        isLeaf: true,
        field: "field_3",
    },
}

const Node = ({ tree, nodeId }) => {
    const node = tree[nodeId]
    return ([
        <li key="node">
            { node.name }
        </li>,
        !node.isLeaf &&
            <ul key="children">
                { map(node.children, (childId) => (
                    <Node key={childId} tree={tree} nodeId={childId}/>
                ))}
            </ul>
    ])
}

const Tree = ({tree}) => (
    <ul>
        { map(tree[0].children, (childId) => (
            <Node key={childId} tree={tree} nodeId={childId}/>
        ))}
    </ul>
)

class Mockup extends Component {
    render() {
        return (
            <div>
                <Row>
                    <Col md={6}>
                        <h5 className="clearfix">
                            Pivot
                            <button className="pull-right">
                                Go
                            </button>
                        </h5>

                        <Table bordered>
                            <tbody>
                                <tr>
                                    <th>Row Field</th>
                                    <td>Leaf 1.1.1</td>
                                </tr>
                                <tr>
                                    <th>Col Field</th>
                                    <td>Leaf 2.2</td>
                                </tr>
                            </tbody>
                        </Table>

                        <Table bordered>
                            <thead>
                                <tr>
                                    <th>{pivot.table}</th>
                                    { map(pivot.cols, (level) => (
                                        <th key={level}>{level}</th>
                                    ))}
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                { map(pivot.rows, (level, row_no) => (
                                    <tr key={row_no}>
                                        <th>{level}</th>
                                        { map(pivot.data[row_no], (value, col_no) => (
                                            <td key={col_no}>{value}</td>
                                        ))}
                                        <td>{pivot.totals.rows[row_no]}</td>
                                    </tr>
                                ))}
                                <tr>
                                    <th>Total</th>
                                    { map(pivot.totals.cols, (value, col_no) => (
                                        <th key={col_no}>{value}</th>
                                    ))}
                                    <th>{pivot.totals.grand}</th>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                    <Col>
                        <h5>Fields</h5>
                        <Tree tree={fieldTree}/>
                    </Col>
                </Row>

            </div>
        )
    }
}

export default Mockup
