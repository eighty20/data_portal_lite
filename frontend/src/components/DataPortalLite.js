import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'

import PivotPanel from './PivotPanel'
import FieldPanel from './FieldPanel'

class DataPortalLite extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rowField: null,
            rowFieldDesc: null,
            colField: null,
            colFieldDesc: null,
        }
    }

    selectField = (fieldType, field, fieldDesc) => {
        if (fieldType === "row") {
            this.setState({
                rowField: field,
                rowFieldDesc: fieldDesc
            })
        }
        else if (fieldType === "col") {
            this.setState({
                colField: field,
                colFieldDesc: fieldDesc
            })
        }
    }

    render() {
        const { rowField, colField, rowFieldDesc, colFieldDesc } = this.state
        return (
            <div>
                <Row>
                    <Col md={6}>
                        <PivotPanel
                            rowField={rowField}
                            rowFieldDesc={rowFieldDesc}
                            colField={colField}
                            colFieldDesc={colFieldDesc}
                        />
                    </Col>
                    <Col>
                        <FieldPanel onSelectField={this.selectField}/>
                    </Col>
                </Row>

            </div>
        )
    }
}

export default DataPortalLite
