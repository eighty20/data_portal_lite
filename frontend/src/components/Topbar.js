import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navbar, NavbarBrand, Nav, NavItem, NavLink, NavbarToggler, Collapse } from 'reactstrap'
import { Link } from 'react-router-dom'

import * as actions from '../actions'

class Topbar extends Component {
    constructor(props) {
        super(props)
        this.state  = {
            loggedIn: false,
            user_id: "",
            user_name: "",
            collapsed: true
        }
    }

    logoutHandler = () => {
        const { clearAlerts, logoutUser } = this.props
        clearAlerts()
        logoutUser()
        this.toggleNavbar()
    }

    toggleNavbar = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    render() {
        const { loggedIn, loggedInUser } = this.props
        return (
            <Navbar key="nav" color="dark" dark>
                <NavbarBrand tag={Link} to="/">Eighty20 App</NavbarBrand>
                <NavbarToggler onClick={this.toggleNavbar} className="mr-2"/>
                <Collapse isOpen={!this.state.collapsed} navbar>
                    <Nav navbar>
                        <NavItem>
                            <NavLink tag={Link} to="/" onClick={this.toggleNavbar}>Home</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to="/mockup" onClick={this.toggleNavbar}>Mockup</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={Link} to="/dataPortalLite" onClick={this.toggleNavbar}>Data Portal Lite</NavLink>
                        </NavItem>
                    </Nav>
                    <hr/>
                    { loggedIn ?
                        <Nav navbar>
                            <NavItem className="text-white">
                                {loggedInUser.first_name} <i className="fas fa-user"/>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to="/account" onClick={this.toggleNavbar}>Account</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} to="/login" onClick={this.logoutHandler}>Logout</NavLink>
                            </NavItem>
                        </Nav> :
                        <Nav navbar>
                            <NavItem>
                                <NavLink tag={Link} to="/login" onClick={this.toggleNavbar}>Login</NavLink>
                            </NavItem>
                        </Nav>
                    }
                </Collapse>
            </Navbar>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const { loggedIn, loggedInUser } = state.auth
    const { users } = state.entities
    return {
        loggedIn,
        loggedInUser: users[loggedInUser]
    }
}



export default connect(mapStateToProps, actions)(Topbar)
