export * from './auth'
export * from './data'

export const CLEAR_ALERTS = 'CLEAR_ALERTS'

export const clearAlerts = () => ({
    type: CLEAR_ALERTS,
})
