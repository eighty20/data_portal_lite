import { backendHost } from './backend'
import { clearAlerts } from '.'

export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST'
export const FETCH_DATA_RESPONSE = 'FETCH_DATA_RESPONSE'
export const FETCH_DATA_ERROR = 'FETCH_DATA_ERROR'

export const fetchData = (data) => {
    return (dispatch) => {
        dispatch(fetchDataRequest())
        const url = `${backendHost}/data`
        return fetch(url, {
            method: 'GET',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        }).then(response => response.json())
        .then(response => {
            if (response && response.success) {
                dispatch(fetchDataResponse(response))
            }
            else {
                dispatch(fetchDataError(response.message))
            }
            setTimeout(() => {
                dispatch(clearAlerts())
            }, 5000)
        })
        .catch(error => {
            dispatch(fetchDataError(error.message))
        })
    }
}

export const fetchDataRequest = () => ({
    type: FETCH_DATA_REQUEST,
    message: "Fetching data",
})

export const fetchDataResponse = (response) => ({
    type: FETCH_DATA_RESPONSE,
    response: response.payload,
    message: response.message

})

export const fetchDataError = (message) => ({
    type: FETCH_DATA_ERROR,
    message
})
