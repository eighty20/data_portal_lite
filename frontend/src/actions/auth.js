import { backendHost } from './backend'

export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST'
export const REGISTER_USER_RESPONSE = 'REGISTER_USER_RESPONSE'
export const REGISTER_USER_ERROR = 'REGISTER_USER_ERROR'
export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST'
export const LOGIN_USER_RESPONSE = 'LOGIN_USER_RESPONSE'
export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR'
export const LOGOUT_USER = 'LOGOUT_USER'
export const EDIT_USER_REQUEST = 'EDIT_USER_REQUEST'
export const EDIT_USER_RESPONSE = 'EDIT_USER_RESPONSE'
export const EDIT_USER_ERROR = 'EDIT_USER_ERROR'
export const CHANGE_PASSWORD_REQUEST = 'CHANGE_PASSWORD_REQUEST'
export const CHANGE_PASSWORD_RESPONSE = 'CHANGE_PASSWORD_RESPONSE'
export const CHANGE_PASSWORD_ERROR = 'CHANGE_PASSWORD_ERROR'

export const registerUser = (data) => {
    return (dispatch) => {
        dispatch(registerUserRequest(data.email))
        const url = `${backendHost}/users`
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        }).then(res => res.json())
        .catch(error => { dispatch(registerUserError(error.message)) })
        .then(response => {
            if (response.success) {
                dispatch(registerUserResponse(response))
            }
            else {
                dispatch(registerUserError(response.message))
            }
        })
    }
}

export const registerUserRequest = (email) => ({
    type: REGISTER_USER_REQUEST,
    email,
    message: "Attempting to register user",
})

export const registerUserResponse = (response) => ({
    type: REGISTER_USER_RESPONSE,
    response: response.payload,
    message: response.message

})

export const registerUserError = (message) => ({
    type: REGISTER_USER_ERROR,
    message
})

export const loginUser = (data) => {
    return (dispatch) => {
        dispatch(loginUserRequest(data.email))
        const url = `${backendHost}/login`
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        }).then(res => res.json())
        .catch(error => { dispatch(loginUserError(error.message)) })
        .then(response => {
            if (response.success) {
                dispatch(loginUserResponse(response))
            }
            else {
                dispatch(loginUserError(response.message))
            }
        })
    }
}

export const loginUserRequest = (email) => ({
    type: LOGIN_USER_REQUEST,
    email,
    message: "Attempting Login"
})

export const loginUserResponse = (response) => ({
    type: LOGIN_USER_RESPONSE,
    response: response.payload,
    message: response.message
})

export const loginUserError = (message) => ({
    type: LOGIN_USER_ERROR,
    message
})

export const logoutUser = () => ({
    type: LOGOUT_USER,
})

export const editUser = (data) => {
    return (dispatch) => {
        dispatch(editUserRequest())
        const url = `${backendHost}/user/id/${data.id}`
        return fetch(url, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        }).then(res => res.json())
        .catch(error => { dispatch(editUserError(error.message)) })
        .then(response => {
            if (response.success) {
                dispatch(editUserResponse(response))
            }
            else {
                dispatch(editUserError(response.message))
            }
        })
    }
}

export const editUserRequest = () => ({
    type: EDIT_USER_REQUEST,
    message: "Attempting to edit user details"
})

export const editUserResponse = (response) => ({
    type: EDIT_USER_RESPONSE,
    response: response.payload,
    message: response.message
})

export const editUserError = (message) => ({
    type: EDIT_USER_ERROR,
    message
})

export const changePassword = (data) => {
    return (dispatch) => {
        dispatch(changePasswordRequest())
        const url = `${backendHost}/user/id/${data.id}`
        return fetch(url, {
            method: 'PUT',
            body: JSON.stringify(data),
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        }).then(res => res.json())
        .catch(error => { dispatch(changePasswordError(error.message)) })
        .then(response => {
            if (response.success) {
                dispatch(changePasswordResponse(response))
            }
            else {
                dispatch(changePasswordError(response.message))
            }
        })
    }
}

export const changePasswordRequest = () => ({
    type: CHANGE_PASSWORD_REQUEST,
    message: "Attempting to edit user details"
})

export const changePasswordResponse = (response) => ({
    type: CHANGE_PASSWORD_RESPONSE,
    response: response.payload,
    message: response.message
})

export const changePasswordError = (message) => ({
    type: CHANGE_PASSWORD_ERROR,
    message
})
