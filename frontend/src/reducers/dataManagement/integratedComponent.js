import { combineReducers } from 'redux'

import {
    FETCH_DATA_REQUEST, FETCH_DATA_ERROR, FETCH_DATA_RESPONSE,
    CLEAR_ALERTS
} from '../../actions'

const status = (state="", action) => {
    switch (action.type) {
        case FETCH_DATA_REQUEST:
            return "request"
        case FETCH_DATA_ERROR:
            return "error"
        case FETCH_DATA_RESPONSE:
            return "response"
        case CLEAR_ALERTS:
            return ""
        default:
            return state
    }
}

const message = (state="", action) => {
    switch (action.type) {
        case FETCH_DATA_REQUEST:
        case FETCH_DATA_ERROR:
        case FETCH_DATA_RESPONSE:
            return action.message
        case CLEAR_ALERTS:
            return ""
        default:
            return state
    }
}

export default combineReducers({
    status,
    message
})
