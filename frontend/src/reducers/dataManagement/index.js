import { combineReducers } from 'redux'

import login from './login'
import register from './register'
import account from './account'
import integratedComponent from './integratedComponent'

export default combineReducers({
    login,
    register,
    account,
    integratedComponent
})
