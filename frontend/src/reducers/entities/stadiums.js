import {
    FETCH_DATA_RESPONSE,
} from '../../actions'

export default (state={}, action) => {
    switch (action.type) {
        case FETCH_DATA_RESPONSE:
            return action.response.entities.stadiums
        default:
            return state
    }
}
