import {
    LOGIN_USER_RESPONSE,
    EDIT_USER_RESPONSE
} from '../../actions'

export default (state={}, action) => {
    switch (action.type) {
        case LOGIN_USER_RESPONSE:
        case EDIT_USER_RESPONSE:
            return action.response.entities.users
        default:
            return state
    }
}
