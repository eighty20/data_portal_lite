import { combineReducers } from 'redux'

import users from './users'
import teams from './teams'
import matches from './matches'
import stadiums from './stadiums'

export default combineReducers({
    users,
    teams,
    matches,
    stadiums
})
