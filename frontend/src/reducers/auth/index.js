import { combineReducers } from 'redux'

import {
    LOGIN_USER_RESPONSE, LOGOUT_USER
} from '../../actions'

const loggedIn = (state=false, action) => {
    switch (action.type) {
        case LOGIN_USER_RESPONSE:
            return true
        case LOGOUT_USER:
            return false
        default:
            return state
    }
}

const loggedInAdmin = (state=false, action) => {
    switch (action.type) {
        case LOGIN_USER_RESPONSE:
            const id = action.response.result
            const { is_admin } = action.response.entities.users[id]
            return is_admin
        case LOGOUT_USER:
            return false
        default:
            return state
    }
}

const loggedInUser = (state="", action) => {
    switch (action.type) {
        case LOGIN_USER_RESPONSE:
            return action.response.result
        case LOGOUT_USER:
            return ""
        default:
            return state
    }
}

const loginToken = (state="", action) => {
    switch (action.type) {
        case LOGIN_USER_RESPONSE:
            return action.response.entities.tokens[action.response.result]
        case LOGOUT_USER:
            return ""
        default:
            return state
    }
}

export default combineReducers({
    loggedIn,
    loggedInAdmin,
    loggedInUser,
    loginToken
})
