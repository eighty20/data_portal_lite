#!/usr/bin/env bash

docker build -t worldcup/backend:latest .
docker run -d --env-file=.env --name=worldcup_backend --net=host --rm worldcup/backend
