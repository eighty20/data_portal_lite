from server import db

db_type = db.session.bind.dialect.name
if db_type == 'postgresql':
    db.session.execute("DROP SCHEMA public CASCADE")
    db.session.execute("CREATE SCHEMA public")
    db.session.execute("GRANT ALL ON SCHEMA public TO postgres")
    db.session.execute("GRANT ALL ON SCHEMA public TO public")
    db.session.execute("COMMENT ON SCHEMA public IS 'standard public schema'")
    db.session.commit()
else:
    db.reflect()
    db.drop_all()
# db.create_all()
