from datetime import datetime
import pandas as pd

from server import db
from server.models import Team, Match, Tournament, Stadium, User

tournaments = pd.read_csv("data/tournament.csv")
for index, row in tournaments.iterrows():
    tournament = Tournament(
        name=row["name"],
        year=row["year"],
        start_date=datetime.strptime(row["start_date"], "%Y-%m-%d"),
        end_date=datetime.strptime(row["end_date"], "%Y-%m-%d"),
        location=row["location"],
    )
    db.session.add(tournament)
db.session.commit()

stadiums = pd.read_csv("data/stadium.csv")
for index, row in stadiums.iterrows():
    stadium = Stadium(
        name=row["name"],
        location=row["location"]
    )
    db.session.add(stadium)
db.session.commit()

teams = pd.read_csv("data/team.csv")
for index, row in teams.iterrows():
    team = Team(
        name=row["name"],
        code=row["code"],
        iso_code=row["iso_code"],
        flag_url=row["flag_url"],
        lat=row["lat"],
        lng=row["lng"]
    )
    db.session.add(team)
db.session.commit()

matches = pd.read_csv("data/match.csv")
for index, row in matches.iterrows():
    tournament = db.session.query(Tournament).filter_by(name="FIFA World Cup", year=2018).first()
    home_team = db.session.query(Team).filter_by(name=row["home_team_name"]).first()
    away_team = db.session.query(Team).filter_by(name=row["away_team_name"]).first()
    stadium = db.session.query(Stadium).filter_by(name=row["stadium_name"]).first()
    match = Match(
        date=datetime.strptime(row["date"], "%Y-%m-%d").date(),
        time=datetime.strptime(row["time"], "%H:%M").time(),
        tournament=tournament,
        tournament_match_no=row["tournament_match_no"],
        tournament_stage=row["tournament_stage"],
        stadium=stadium,
        home_team=home_team,
        away_team=away_team,
        complete=True,
        home_team_score=row["home_team_score"],
        away_team_score=row["away_team_score"]
    )
    db.session.add(match)
db.session.commit()

admin_user = User(
    first_name="Admin",
    last_name="User",
    email="admin@eighty20.co.za",
    is_admin=True
)
admin_user.set_password("admin")
db.session.add(admin_user)
db.session.commit()
