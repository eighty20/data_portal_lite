import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    DB_TYPE = os.getenv('DB_TYPE', 'sqlite')
    DB_USER = os.getenv('DB_USER', 'postgres')
    DB_PASSWORD = os.getenv('DB_PASSWORD', 'postgres')
    DB_HOST = os.getenv('DB_HOST', 'localhost')
    DB_PORT = os.getenv('DB_PORT', '5432')
    DB_DATABASE = os.getenv('DB_DATABASE', 'worldcup')
    if DB_TYPE == 'postgresql':
        SQLALCHEMY_DATABASE_URI = '{DB_TYPE}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_DATABASE}'\
            .format(DB_TYPE=DB_TYPE, DB_USER=DB_USER, DB_PASSWORD=DB_PASSWORD,
                DB_HOST=DB_HOST, DB_PORT=DB_PORT, DB_DATABASE=DB_DATABASE)
    elif DB_TYPE == 'mysql':
        SQLALCHEMY_DATABASE_URI = '{DB_TYPE}://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_DATABASE}'\
            .format(DB_TYPE=DB_TYPE, DB_USER=DB_USER, DB_PASSWORD=DB_PASSWORD,
                DB_HOST=DB_HOST, DB_PORT=DB_PORT, DB_DATABASE=DB_DATABASE)
    else:
        SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')

    SQLALCHEMY_TRACK_MODIFICATIONS = False
