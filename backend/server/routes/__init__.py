from flask import request

from server import app
from server.routes import users, fields, pivots


@app.before_request
def log_request_info():
    if request.method not in ["OPTIONS", "GET"]:
        app.logger.info('Request Body:' + request.get_data().decode('utf-8'))


@app.after_request
def log_response_info(response):
    if request.method != "OPTIONS":
        app.logger.info('Response Body:' + response.get_data().decode('utf-8'))
    return(response)
