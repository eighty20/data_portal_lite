from flask import request, jsonify, url_for

from server import app, db
from server.methods.errors import bad_request
from server import methods


@app.route("/user/id/<int:id>", methods=["GET"])
def get_user_by_id(id):
    user = methods.users.get_user_by_id(id)
    if user is None:
        return bad_request("User not found")
    else:
        response = jsonify({
            "success": True,
            "message": "User {} successfully retrieved".format(user.id),
            "payload": {
                "entities": {
                    "users": {
                        user.id: user.to_dict(),
                    },
                },
                "result": user.id,
            }
        })
        response.headers['Location'] = url_for('.get_user_by_id', id=user.id)
        return response


@app.route("/user/id/<int:id>", methods=["PUT"])
def update_user(id):
    data = request.get_json() or {}
    user = methods.users.get_user_by_id(id)
    if user is None:
        return bad_request("User not found")
    else:
        if "old_password" in data and "new_password" in data:
            if user.check_password(data["old_password"]):
                user.set_password(data["new_password"])
            else:
                return bad_request("Old password incorrect")
        user.from_dict(data)
        db.session.add(user)
        db.session.commit()
        response = {
            "success": True,
            "message": "User {} successfully updated".format(user.email),
            "payload": {
                "entities": {
                    "users": {
                        user.id: user.to_dict(),
                    },
                },
                "result": user.id,
            },
        }
        response = jsonify(response)
        response.status_code = 202
        response.headers['Location'] = url_for('.get_user_by_id', id=user.id)
        return response


@app.route("/user/email/<string:email>", methods=["GET"])
def get_user_by_email(email):
    user = methods.users.get_user_by_email(email)
    if user is None:
        return bad_request("User not found")
    else:
        return jsonify({
            "success": True,
            "message": "User {} successfully retrieved".format(user.email),
            "payload": {
                "entities": {
                    "users": {
                        user.id: user.to_dict(),
                    },
                },
                "result": user.id,
            }
        })


@app.route("/user/email/<string:email>", methods=["PUT"])
def reset_password(email):
    data = request.get_json() or {}
    user = methods.users.get_user_by_email(email)
    if user is None:
        return bad_request("User not found")
    else:
        user.set_password(data.password)
        db.session.add(user)
        db.session.commit()
        response = {
            "success": True,
            "message": "User {} successfully reset password".format(user.email),
            "payload": {
                "entities": {
                    "users": {
                        user.id: user.to_dict(),
                    },
                },
                "result": user.id,
            },
        }
        response = jsonify(response)
        response.status_code = 202
        response.headers['Location'] = url_for('.get_user_by_id', id=user.id)
        return response


@app.route("/users", methods=["POST"])
def register_user():
    data = request.get_json() or {}
    if "first_name" not in data or "email" not in data or "password" not in data:
        return bad_request("Must include first name, email and password")
    elif methods.users.get_user_by_email(data["email"]) is not None:
        return bad_request("User email already exists")
    else:
        user = methods.users.register_user(data)
        db.session.commit()
        response = {
            "success": True,
            "message": "User {} successfully created".format(user.email),
            "payload": {
                "entities": {
                    "users": {
                        user.id: user.to_dict(),
                    },
                },
                "result": user.id,
            },
        }
        response = jsonify(response)
        response.status_code = 201
        response.headers['Location'] = url_for('.get_user_by_id', id=user.id)
        return response


@app.route("/login", methods=["POST"])
def login_user():
    data = request.get_json() or {}
    if "email" not in data or "password" not in data:
        return bad_request("Must submit user email and password")
    else:
        user = methods.users.get_user_by_email(data["email"])
        if user is None:
            return bad_request("User does not exist")
        elif not user.check_password(data["password"]):
            return bad_request("Wrong password")
        else:
            response = {
                "success": True,
                "message": "User {} successfully logged in".format(user.email),
                "payload": {
                    "entities": {
                        "tokens": {
                            user.id: methods.users.generate_token(user),
                        },
                        "users": {
                            user.id: user.to_dict(),
                        },
                    },
                    "result": user.id,
                },
            }
            response = jsonify(response)
            response.status_code = 200
            response.headers['Location'] = url_for('.get_user_by_id', id=user.id)
            return response
