from flask import request, jsonify, url_for

from server import app, db
from server.methods.errors import bad_request
from server import methods

@app.route("/pivot", methods=["GET"])
def get_pivot():
    # try:
        database = request.args.get("database")
        table = request.args.get("table")
        row_field = request.args.get("row_field")
        col_field = request.args.get("col_field")
        pivot = methods.pivots.get_pivot(database, table, row_field, col_field)

        response = {
            "success": True,
            "message": "Fetched pivot",
            "payload": {
                "entities": {
                    "pivot": pivot,
                },
                "result": {
                    "pivot": list(pivot.keys()),
                }
            },
        }
        response = jsonify(response)
        response.status_code = 200
        response.headers['Location'] = url_for('.get_pivot', database=database, table=table, row_field=row_field, col_field=col_field)
        return response
    # except Exception as e:
    #     return bad_request("something bad happened")
