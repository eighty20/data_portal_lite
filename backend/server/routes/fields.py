from flask import request, jsonify, url_for

from server import app, db
from server.methods.errors import bad_request
from server import methods

@app.route("/fieldTree", methods=["GET"])
def get_field_tree():
    try:
        database = request.args.get("database")
        table = request.args.get("table")
        field_tree = methods.fields.get_field_tree(database, table)

        response = {
            "success": True,
            "message": "Fetched field tree",
            "payload": {
                "entities": {
                    "field_tree": field_tree,
                },
                "result": {
                    "field_tree": list(field_tree.keys()),
                }
            },
        }
        response = jsonify(response)
        response.status_code = 200
        response.headers['Location'] = url_for('.get_field_tree', database=database, table=table)
        return response
    except Exception as e:
        return bad_request("something bad happened")
