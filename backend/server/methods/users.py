from server import db
from server.models import User
from datetime import datetime, timedelta
import json


def get_user_by_id(id):
    user = User.query.get(id)
    return user


def get_user_by_email(email):
    user = User.query.filter_by(email=email).first()
    return user


def register_user(data):
    user = User()
    user.from_dict(data, new_user=True)
    db.session.add(user)
    return user


def login_user(email, password):
    user = User.query.filter_by(email=email)
    if user is None or not user.check_password(password):
        return None
    else:
        return generate_token(user)


def generate_token(user):
    return json.dumps({
        'exp': str(datetime.utcnow() + timedelta(days=0, seconds=60)),
        'iat': str(datetime.utcnow()),
        'sub': user.id,
    })


def change_password(user, password):
    user.set_password(password)
    db.session.add(user)
