from server import db
import pandas as pd
from textwrap import dedent as d


def add_field_to_tree(field, field_desc, branches, tree):
    if len(branches) > 0:
        node = branches.pop(0)
        if node not in tree["branches"]:
            tree["branches"][node] = {
                "leaves": [],
                "branches": {}
            }
        add_field_to_tree(field, field_desc, branches, tree["branches"][node])
    else:
        tree["leaves"].append({
            "field": field,
            "field_desc": field_desc
        })


def normalise_tree(tree, output_tree, next_id=0, parent_id=None):
    branch_node_ids = []
    for branch in sorted(tree["branches"]):
        output_tree[next_id] = {
            "name": branch,
            "isLeaf": False,
            "children": []
        }
        branch_node_ids.append(next_id)
        if parent_id is not None:
            output_tree[parent_id]["children"].append(next_id)
        next_id += 1
    for leaf in sorted(tree["leaves"], key=lambda x: x["field_desc"]):
        output_tree[next_id] = {
            "name": leaf["field_desc"],
            "isLeaf": True,
            "field": leaf["field"]
        }
        if parent_id is not None:
            output_tree[parent_id]["children"].append(next_id)
        next_id += 1
    for i, branch in enumerate(sorted(tree["branches"])):
        next_id = normalise_tree(tree["branches"][branch], output_tree, next_id, branch_node_ids[i])
    return next_id


def get_field_tree(database, table):
    query = db.text(d("""
        SELECT *
        FROM eighty20_databaseadmin.translation_db
        WHERE databasename = :database
        AND db_table = :table
        ORDER BY field_desc
    """)).bindparams(database=database, table=table)
    translations = pd.read_sql(query, db.engine)

    translations = translations[translations["branches"].str.startswith("Root")]
    translations = translations[~translations["levels"].str.startswith("Number")]
    translations = translations[~translations["levels"].str.startswith("Value")]

    fields = {}
    for i, translation in translations.iterrows():
        fields[translation["field"]] = {
            "field": translation["field"],
            "field_desc": translation["field_desc"],
            "branches": translation["branches"].split("~")
        }

    field_tree = {
        "leaves": [],
        "branches": {}
    }

    for field_name in fields:
        field = fields[field_name]
        add_field_to_tree(field["field"], field["field_desc"], field["branches"], field_tree)

    output_tree = {}
    normalise_tree(field_tree, output_tree)
    return output_tree
