from server import db
import pandas as pd
from textwrap import dedent as d


def translation_to_dict(translation):
    return {
        "field": translation["field"],
        "field_desc": translation["field_desc"],
        "levels": {
            level: level_label for level, level_label in zip(
                translation["levels"].split("||"),
                translation["level_labels"].split("||")
            )
        },
        "branches": translation["branches"].split("~")
    }


def get_field_dict(database, table, field):
    query = db.text(d("""
        SELECT field, field_desc, levels, level_labels, branches
        FROM eighty20_databaseadmin.translation_db
        WHERE databasename = :database
        AND db_table = :table
        AND field = :field
    """)).bindparams(database=database, table=table, field=field)
    translation = pd.read_sql(query, db.engine).loc[0]
    return translation_to_dict(translation)


def get_pivot(database, table, row_field, col_field):
    query = db.text(d(f"""
        SELECT weight
        FROM eighty20_databaseadmin.tables_meta
        WHERE db = :database
        AND db_table = :table
    """)).bindparams(database=database, table=table)
    weight_field = pd.read_sql(query, db.engine).iat[0,0]

    query = d(f"""
        SELECT {row_field} AS row, {col_field} AS col, ROUND(SUM({weight_field})) as individuals
        FROM {database}.{table}
        GROUP BY {row_field}, {col_field}
    """)
    df = pd.read_sql(query, db.engine)

    pivot_df = df.pivot(index="row", columns="col", values="individuals")
    pivot_dict = pivot_df.to_dict('split')

    row_trans = get_field_dict(database, table, row_field)
    col_trans = get_field_dict(database, table, col_field)
    print(pivot_dict["index"])

    pivot = {
        "rows": [row_trans["levels"][level] if not level == "None" else "N/A" for level in pivot_dict["index"]],
        "cols": [col_trans["levels"][level] if not level == "None" else "N/A" for level in pivot_dict["columns"]],
        "data": pivot_dict["data"],
        "totals": {
            "rows": [sum(row) for row in pivot_dict["data"]],
            "cols": [sum(col) for col in zip(*pivot_dict["data"])],
            "grand": sum([sum(row) for row in pivot_dict["data"]])
        }
    }
    return pivot
