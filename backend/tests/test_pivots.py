import pytest
from server import methods
import json

def test_get_field_tree():
    database = "labour_fs"
    table = "lfs2018q1"
    row_field = "Education_Status"
    col_field = "Q13GENDER"

    field_tree = methods.pivots.get_pivot(database, table, row_field, col_field)
    field_tree = json.loads(json.dumps(field_tree))

    with open("tests/data/lfs2018q1_pivot.json", "r") as f:
        template_field_tree = json.load(f)

    assert field_tree == template_field_tree
