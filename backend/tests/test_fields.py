import pytest
from server import methods
import json

def test_get_field_tree():
    database = "labour_fs"
    table = "lfs2018q1"

    field_tree = methods.fields.get_field_tree(database, table)
    field_tree = json.loads(json.dumps(field_tree))

    with open("tests/data/lfs2018q1_field_tree.json", "r") as f:
        template_field_tree = json.load(f)

    assert field_tree == template_field_tree
