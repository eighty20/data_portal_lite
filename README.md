# Eighty20 Prototype App Template

This project is intended to be a template, that can be forked off to form the base of any new web app based data product prototyped at Eighty20. The technologies used have been selected for their ease of startup and use to get a proof of concept up quickly, but are not necessarily the best choices for a final production system.

## Quickstart (Ubuntu)

1. Clone Project

        git clone git@bitbucket.org:eighty20/app_template.git new_project
        cd new_project

1. Setup Python Virtual Environment

        python3.6 -m venv venv
        source venv/bin/activate
        cd backend
        pip install -r requirements.txt
        cp .env.template .env

1. Setup database (assumes a postgresql server running on localhost with admin user=postgres and password=postgres)

        sudo -u postgres createdb worldcup
        flask db upgrade
        python populate_tables.py

1. Run Backend server

        flask run

1. Install frontend packages (open a new terminal)

        cd new_project/frontend
        yarn

1. Setup local copies of environment and config files
        cp .env.template .env
        cp nginx.local.conf nginx.conf

1. Run frontend server

        yarn start


## Contents

This document is laid out as follows:

- #### Instructions for setting up a new project based on this one
    - Forking the repository
    - Setting up a dev environment
    - Adding in new functionality
    - Continuous integration including version control and tests
    - Testing deployment locally
    - Deploying on an internal server for use on the Eighty20 internal network only
    - Deploying on an internal server for outside access
    - Deploying on an cloud based server
    - Making changes to the live deployed environment
    - Advice for scaling up to a full production solution
- #### Description of the different components that make up the full stack
    - React Javascript based Frontend
    - Python Flask based API backend
    - Postgresql Database
    - Nginx web server
    - Git version control
    - Docker containers for deployment
    - Things that aren't in here yet but probably should be included at some point
        - R API backend
        - JSON Web Token (JWT) based authentication
        - ELK Stack (ElasticSearch, Logstash, Kibana) for logging
        - Bitbucket Pipelines for continuous integration
        - Load balancing
        - Real time model updates
- #### Development Best Practices
    - Initial design, planning and specifications
    - Core functionality, bugs and additional features
    - Project tracking
    - Premature optimisation is the root of all evil
    - Move fast and break things (but `git commit` first)
    - Write unit and integration tests (red, green, refactor)
    - Documentation
    - Pull request minimum requirements
    - Style guide

## New project Instructions

### Setup Bitbucket to use SSH

To make our lives easier, we'll first set up Bitbucket to use the SSH protocol instead of HTTPS. The major benefit of this is that we won't need to keep entering user names and passwords for git. It also helps when deploying the app on remote servers as the repository url won't be linked to any particular user's name.

Follow the instructions [here](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html) to add an ssh key to Bitbucket. You'll need to do this for any PC you want to able to work from.

### Fork the repository

Go to the [Bitbucket page](https://bitbucket.org/eighty20/app_template/src/master/) for this repository (you're probably viewing this readme there at the moment). Click the **+** icon on the left sidebar, and select **Fork this repository** from the options.

This will bring up fork project dialog. Change the owner to **eighty20**, unless you really just want to use it yourself, give it a new name and click **fork repository**

You'll be taken to the page for your new repository. Click `Clone` in the top right hand corner and run that command in a suitable location. You'll need to at least have git set up, so follow at least those instructions below if you haven't already.

### Set up your dev environment

This assumes that you're running Ubuntu 18.04 or later and are using the apt package manager. If you're running windows, follow these [instructions](https://statslab.eighty20.co.za/posts/windows_dev_environment/) to set up a working windows dev enviroment.

Replace `/path/to/app` below with the actual path that you've cloned the repository to. If you're already in this folder, you can remove this part from all the file paths.

1. #### Install Python 3.6 and set up a virtual environment

        sudo apt install python3.6 python3.6-venv

    Create a new virtual environment for your application

        cd /path/to/app
        python3.6 -m venv venv

    This will automatically enter the virtual environment once its created. To exit the virtual environment run

        deactivate

    Whenever you want to get into the virtual environment from a new terminal, run

        source /path/to/app/venv/bin/activate

    Install the required packages into the virtual Environment

        pip install /path/to/app/backend/requirements.txt

1. #### Install node and yarn

        sudo apt install nodejs yarn

    Install the frontend packages

        cd /path/to/app/frontend
        yarn

1. #### Install postgresql server

        sudo apt install postgresql postgresql-contrib

    If the server isn't running start it with

        sudo service postgresql start

    or

        sudo systemctl start postgresql

    Start a new postgres session and set the password for the postgres user. For now we've set it to also be `postgres` but this should be changed in actual production

        sudo -u postgres psql
        \password postgres
        postgres
        \q

    Create the db we'll be using for this template app

        sudo -u postgres createdb worldcup

    Create and populate the tables with these scripts

        cd /path/to/app/backend
        source /path/to/app/venv/bin/activate
        flask db upgrade
        python populate_tables.py

### Run the backend development server

Launch a new terminal and go to the backend folder

    cd /path/to/app/backend

Create a new `.env` file from the template

    cp .env.template .env

Enter the virtual environment and launch the server

    source /path/to/app/venv/bin/activate
    flask run

This will start a development server on port 3000

### Run the frontend development server

Launch a new terminal and go to the frontend folder

    cd /path/to/app/frontend

Create a new `.env` file from the template

    cp .env.template .env

Enter the virtual environment and launch the server

    yarn start

This will start a development server on port 5000



### Add in new functionality

### Test your new functionality

### Commit to version control

### Test deployment locally

### Deploy on internal server for internal use

### Deploy on internal server for external use

### Deploy on external server (The Cloud)

### Updates to live deployment

### Scale up and scale out

## Full Stack Breakdown

Modern web applications are quite complex creatures. The goal is ostensibly to break everything down into standalone microservices that can be chopped and changed at will, using the latest and greatest technologies, that can be scaled up or down on demand and continue to work seamlessly as new features are dropped in. The guiding principles are well covered in the [12 Factor App](https://12factor.net).

That's all well and good in theory, but the reality is a lot of moving parts with a host of different programming languages, libraries and frameworks that are intricately wired together, with only a few people who actually understand the whole picture.

I'm going to try in this section to outline each of these components at a high level, the purpose they play in the overall function of the app and why we've gone with the particular technology in this template. The intention is that once done reading this, you'll have a reasonable idea of how everything fits together, even if you only end up working on a small part of it.

But first a little aside...

### Development vs Production

When I started on this whole software development journey, I'd heard the terms development and production thrown about. And sure development meant the code you were writing and testing on your machine and production was the actual live server that real world users would access, but it wasn't at all clear how, if at all, these should be any different.

Your dev environment is basically your desktop machine (although it is possible to have a remote dev environment). You'll need to have all the tools installed, to write your code, test, debug and then eventually submit it to be included in the live version. You have full control over this environment. You could be running your choice of operating system, you'll likely have your own version of the database with some dummy data and you can even have different versions of some of the software and libraries (though this in not recommended unless you're specifically testing an upgrade). You will be the only user to access any of the services you spin up and they will be accessed on `localhost` through a variety of different ports.

The production server will necessarily be a little different. It will have the minimum setup required to get everything running without the bloat required for debugging and testing. All software and libraries will be stable fixed versions. Because multiple users will now be attempting to access the services simultaneously, you will need multiple instances of your backend workers. Frontend code should be served as *uglified* static files to reduce transfer time. Users will be trying to access your site not via an IP but by a domain name like `app.eighty20.co.za`. The web server is supposed to orchestrate all of this, and while its at it, you probably want to be serving everything over more secure HTTPS rather than plain HTTP. With all these users, you'll also need a good way to log all the requests and responses so that you can figure out what happened if something goes wrong (and to mine the data on how people are actually using your app).

Finally when you release an updated version, you need a way to get this new code onto the server, build as necessary and then replace the running services with updated ones. This is where containerisation and continuous integration come it. Docker containers are also really useful for making sure you can deploy the same code to a bunch of different servers, with all the necessary requirements, even if they have different underlying hardware or operating systems.

All this means that you'll likely have a different setup for your dev environment and your production environment, but these should be designed from the start (as they have been here) to make the transfer as seamless and safe as possible. Even just for a protoype app, it's important to plan for this so that other people can actually use the app as well and see what you've developed. One advantage of being a prototype though, is that we won't need an additional User Acceptance Testing environment - we can test directly on the production prototype. Besides we've already written good unit and integration tests as we've coded haven't we...

### Frontend

Since you've got to start somewhere, I recommend beginning with the frontend. This is how your users will actually interact with whatever service you're trying to provide, and will likely guide whatever functionality you're trying to expose.

The frontend is made up of three parts:
- HTML - the structure and content of the web page
- CSS - the layout and styling of the page
- Javascript - the code that tells the page how to respond to user interaction

We use the [React](https://) framework. It provides an intuitive way to build up your app with individual, reusable components. You write the HTML directly inside the Javascript, where you also describe how the component should ...um react to user events and requests. React was developed by Facebook and has a large and active user community, so is very well supported.

A typical React component looks something like this:

```jsx
import React, { Component} from 'react'
import { map } from 'lodash'

class TodoList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            items = []
        }
    }

    componentDidMount() {
        const url = 'api.eighty20.co.za/todoList/items'
        return fetch(url, {
            method: 'GET',
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            mode: 'cors',
        }).then(res => res.json())
        .then(response => {
            this.setState({
                items: response.items
            })
        })
    }

    render () {
        return (
            <table>
                <thead>
                    <tr>
                        <th>
                            Item
                        </th>
                        <th>
                            Complete
                        </th>
                    </tr>
                </thead>
                <tbody>
                    { map(this.state.items, (item) =>(
                        <tr>
                            <td>{item.name}</td>
                            <td>{item.complete ? "yes" : "no"}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        )
    }
}

export default TodoList
```

### Backend

#### Authentication and Authorisation

### Database

### Web Server

### Version control

### Containerisation

### Logging

### Continuous Integration
